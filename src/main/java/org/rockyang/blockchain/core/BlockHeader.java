package org.rockyang.blockchain.core;

import java.io.Serializable;
import java.math.BigInteger;

import org.rockyang.blockchain.crypto.Hash;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 区块头
 * @author yangjian
 * @since 18-4-6
 */
@Setter
@Getter
@ToString
public class BlockHeader implements Serializable {
 
	private static final long serialVersionUID = 2723424152503727001L;
 
	private Integer index; // 区块高度
 
	private BigInteger difficulty; //难度指标
 
	private Long nonce; //PoW 问题的答案
 
	private Long timestamp; //时间戳
 
	private String hash; //区块头 Hash
 
	private String previousHash; //上一个区块的 hash 地址

	public BlockHeader(Integer index, String previousHash) {
		this.index = index;
		this.timestamp = System.currentTimeMillis();
		this.previousHash = previousHash;
	}

	public BlockHeader() {
		this.timestamp = System.currentTimeMillis();
	}
	/**
	 * 计算当前区块头的 hash 值
	 */
	public String hash() {
		return Hash.sha3("BlockHeader{" +
				"index=" + index +
				", difficulty=" + difficulty +
				", nonce=" + nonce +
				", timestamp=" + timestamp +
				", previousHash='" + previousHash + '\'' +
				'}');
	}
}
