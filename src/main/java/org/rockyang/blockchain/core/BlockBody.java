package org.rockyang.blockchain.core;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.ToString;

/**
 * 区块数据
 * @author yangjian
 * @since 18-4-8
 */
@ToString
public class BlockBody implements Serializable {
 
	private static final long serialVersionUID = 3420392966972003641L;
	 
	private List<Transaction> transactions; //区块所包含的交易记录

	public BlockBody(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	public BlockBody() {
		this.transactions = new ArrayList<>();
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	/**
	 * 添加一笔交易打包到区块 
	 */
	public void addTransaction(Transaction transaction) {
		transactions.add(transaction);
	}
}
