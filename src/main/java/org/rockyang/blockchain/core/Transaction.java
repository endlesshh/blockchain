package org.rockyang.blockchain.core;

import java.math.BigDecimal;

import org.rockyang.blockchain.crypto.Hash;
import org.rockyang.blockchain.enums.TransactionStatusEnum;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 交易对象
 * @author yangjian
 * @since 18-4-6
 */
@Setter
@Getter
@ToString
public class Transaction {
 
	private String from; //付款人地址
 
	private String sign; //付款人签名
 
	private String to;   //收款人地址
 
	private String publicKey; //收款人公钥
	 
	private BigDecimal amount;//交易金额
	 
	private Long timestamp;   //交易时间戳
	 
	private String txHash;    //交易 Hash 值
 
	private TransactionStatusEnum status = TransactionStatusEnum.APPENDING; //交易状态
	 
	private String errorMessage; //交易错误信息
 
	private String data; //附加数据
 
	private int blockNumber; //当前交易所属区块高度

	public Transaction(String from, String to, BigDecimal amount) {
		this.from = from;
		this.to = to;
		this.amount = amount;
		this.timestamp = System.currentTimeMillis();
	}

	public Transaction() {
		this.timestamp = System.currentTimeMillis();
	}
	
	/**
	 * 计算交易信息的Hash值
	 */
	public String hash() {
		return Hash.sha3(this.toSignString());
	}

	/**
	 * 参与签名的字符串
	 * @return
	 */
	public String toSignString() {
		return "Transaction{" +
				"from='" + from + '\'' +
				", to='" + to + '\'' +
				", publicKey=" + publicKey +
				", amount=" + amount +
				", timestamp=" + timestamp +
				", data='" + data + '\'' +
				'}';
	}
}
