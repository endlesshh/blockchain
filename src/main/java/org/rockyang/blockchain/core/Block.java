package org.rockyang.blockchain.core;

import java.io.Serializable;

import lombok.Data;

/**
 * 区块
 * @author yangjian
 * @since 18-4-6
 */
@Data
public class Block implements Serializable {
 
	private static final long serialVersionUID = 2349275228413931271L;
 
	private BlockHeader header; //区块 Header
 
	private BlockBody body; //区块 Body
 
	private int confirmNum = 0; //确认数

	public Block(BlockHeader header, BlockBody body) {
		this.header = header;
		this.body = body;
	}

	public Block(){}
}
