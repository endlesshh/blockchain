package org.rockyang.blockchain.mine.pow;

import java.math.BigInteger;

import lombok.Data;
import lombok.ToString;

/**
 * PoW 计算结果
 * @author yangjian
 */
@Data
@ToString
public class PowResult {

    /**
     * 计数器
     */
    private Long nonce;
    /**
     * 新区快的哈希值
     */
    private String hash;
    /**
     * 目标难度值
     */
    private BigInteger target;

    public PowResult(long nonce, String hash, BigInteger target) {
        this.nonce = nonce;
        this.hash = hash;
        this.target = target;
    }
}
