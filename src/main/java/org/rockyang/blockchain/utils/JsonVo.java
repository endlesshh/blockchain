package org.rockyang.blockchain.utils;

import lombok.Data;

/**
 * 返回 Json 字符串 VO
 * @author yangjian
 * @since 2018-04-07 上午10:56.
 */
@Data
public class JsonVo {

	public static final int CODE_SUCCESS = 200;
	public static final int CODE_FAIL = 400;
 
	private int code; //返回状态码
	 
	private String message; //返回错误信息
	 
	private Object item; //返回数据

	public JsonVo() {}

	public JsonVo(int code, String message, Object item) {
		this.code = code;
		this.message = message;
		this.item = item;
	}

	public static JsonVo instance(int code, String message) {
		return new JsonVo(code, message);
	}

	public JsonVo(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public JsonVo(int code, Object item) {
		this.code = code;
		this.item = item;
	}

	public static JsonVo success() {
		return new JsonVo(CODE_SUCCESS, "SUCCESS", null);
	}

	public static JsonVo fail() {
		return new JsonVo(CODE_FAIL, "FAIL", null);
	}
}
