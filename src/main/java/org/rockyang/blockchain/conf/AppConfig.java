package org.rockyang.blockchain.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * 系统全局配置
 * @author yangjian
 * @since 18-7-14
 */
@Configuration
@EnableConfigurationProperties(AppConfig.class)
@ConfigurationProperties(prefix = "app")
@Data
public class AppConfig {
 
	private boolean nodeDiscover; //是否启用节点发现
 
	private boolean autoMining; //是否自动挖矿
 
	private int minConfirmNum = 0;// 最少确认数
 
	private String dataDir; //数据存储地址
}
