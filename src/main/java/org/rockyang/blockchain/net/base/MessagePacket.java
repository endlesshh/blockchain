package org.rockyang.blockchain.net.base;

import org.tio.core.intf.Packet;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 网络消息数据包
 * @author yangjian
 */
@Data
@EqualsAndHashCode(callSuper=false)
public class MessagePacket extends Packet {
 
	private static final long serialVersionUID = -4574321913381570587L;
 
	public static final int HEADER_LENGTH = 5; //消息头的长度 1+4
 
	public static final String HELLO_MESSAGE = "Hello world."; //打招呼信息
	/** 获取账户列表的消息信号 */
	public static final String FETCH_ACCOUNT_LIST_SYMBOL = "get_accounts_list";
	/**  获取节点列表的消息信号 */
	public static final String FETCH_NODE_LIST_SYMBOL = "get_nodes_list";
	/**  消息类别，类别值在 MessagePacketType 常量类中定义 */
	private byte type;

	private byte[] body;

	public MessagePacket(byte[] body) {
		this.body = body;
	}
	
	public MessagePacket(byte type) {
		this.type = type;
	}
	
	public MessagePacket() {}
}
