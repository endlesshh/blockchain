package org.rockyang.blockchain;

import java.io.File;

import org.rockyang.blockchain.account.Account;
import org.rockyang.blockchain.account.Personal;
import org.rockyang.blockchain.conf.AppConfig;
import org.rockyang.blockchain.db.DBAccess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

/**
 * @author yangjian
 * @since 2019-06-03 下午5:18.
 */
@Component
@Slf4j
public class MyApplicationRunner implements ApplicationRunner{

	@Autowired
	private DBAccess dbAccess;

	@Autowired
	private Personal personal;

	@Autowired
	private AppConfig appConfig;

	@Override
	public void run(ApplicationArguments arguments) throws Exception {

		// 首次运行，执行一些初始化的工作
		File lockFile = new File(System.getProperty("user.dir") + "/" + appConfig.getDataDir() + "/node.lock");
		if (!lockFile.exists()) {
			lockFile.createNewFile();
			// 创建默认钱包地址（挖矿地址）
			Account minerAccount = personal.newAccount();
			dbAccess.setMinerAccount(minerAccount);
			log.info("创建挖矿账号 - Create miner account : {}", minerAccount);
		}

	}

}
