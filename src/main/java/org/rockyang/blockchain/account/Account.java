package org.rockyang.blockchain.account;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

/**
 * 钱包账户
 * @author yangjian
 * @since 18-4-6
 */
@Data
public class Account implements Serializable {
	private static final long serialVersionUID = -6452375319761706800L;
 
	protected String address; //钱包地址
 
	protected String priKey; //钱包私钥
 
	protected BigDecimal balance; //账户余额

	public Account() {}

	public Account(String address, BigDecimal balance) {
		this.address = address;
		this.balance = balance;
	}

	public Account(String address, String priKey, BigDecimal balance) {
		this.address = address;
		this.priKey = priKey;
		this.balance = balance;
	}
}
