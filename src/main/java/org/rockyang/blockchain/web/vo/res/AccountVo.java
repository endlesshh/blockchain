package org.rockyang.blockchain.web.vo.res;

import org.rockyang.blockchain.account.Account;

import lombok.ToString;

/**
 * account VO
 * @author yangjian
 * @since 18-7-14
 */
@ToString
public class AccountVo extends Account { 
	private static final long serialVersionUID = -7047655279173436347L;
 
}
