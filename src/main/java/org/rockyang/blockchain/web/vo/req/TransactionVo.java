package org.rockyang.blockchain.web.vo.req;

/**
 * vo params for sending transaction
 * @author yangjian
 * @since 19-6-9 下午1:57
 */
import java.math.BigDecimal;

import lombok.Data;

/**
 * 发送交易参数 VO
 * @author yangjian
 * @since 18-4-13
 */
@Data
public class TransactionVo{
	/**  收款人地址*/
	private String to;
	/** 交易金额*/
	private BigDecimal amount;
	/** 付款人私钥*/
	private String priKey;
	/** 附加数据 */
	private String data;
}
