package org.rockyang.blockchain.web.vo.req;

import lombok.Data;

/**
 * Node vo params for request
 * @author yangjian
 * @since 19-6-9 下午2:03
 */
@Data
public class NodeVo {
	// ip address
	private String ip;
	// connector port
	private int port;
}
